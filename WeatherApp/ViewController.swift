//
//  ViewController.swift
//  WeatherApp
//
//  Created by Roma on 7/12/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate,MKMapViewDelegate {
    @IBOutlet weak var citySearchBar: UISearchBar!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    
    @IBOutlet weak var mapViewOutlet: MKMapView!
    
        let locationManager = CLLocationManager()
    
  
    @IBAction func mapTapped(_ recognizer: UITapGestureRecognizer) {
        
        let point = recognizer.location(in: mapViewOutlet)
        let tapPoint = mapViewOutlet.convert(point, toCoordinateFrom: mapViewOutlet)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = tapPoint
        
        var locationName: String?
        var temperature: Double?
        var feelsLike: Double?
        mapViewOutlet.removeAnnotations(mapViewOutlet.annotations)
        mapViewOutlet.addAnnotation(annotation)
        let location = CLLocation(latitude: tapPoint.latitude, longitude: tapPoint.longitude)
        
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)  // Rio de Janeiro, Brazil
            self.cityLabel.text = "\(city)"
            let urlString = "https://api.apixu.com/v1/current.json?key=bf21bf36247c4b3a878133001191107&q=\(city)"
            if let url = URL(string: urlString){
            let task = URLSession.shared.dataTask(with: url) {[weak self] (data, responce, error) in
                do {
                    let json = try JSONSerialization.jsonObject(with: data! , options: .mutableContainers) as! [String : AnyObject]
                    if let location = json["location"]  {
                        locationName = location["name"] as? String
                    }
                    if let current = json["current"] {
                        temperature = current["temp_c"] as? Double
                        feelsLike = current["feelslike_c"] as? Double
                        
                    }
                    DispatchQueue.main.async {
                        
                        self?.cityLabel.text = locationName
                        if let temperature = temperature {
                        self?.temperatureLabel.text = "\(temperature)"
                    }
                        if let feelsLike = feelsLike{
                            self?.feelsLikeLabel.text = "\(feelsLike)"
                        }
                        
                    }
                    
                }
                catch let jsonError {
                    print(jsonError)
                }
            }
            task.resume()
           
        }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        citySearchBar.delegate = self
        // Do any additional setup after loading the view.
    }


}

extension ViewController: UISearchBarDelegate {
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchBar.text
        let activeSearch = MKLocalSearch(request: searchRequest)
        activeSearch.start {(responce, error) in
            if responce == nil {
                print("ERROR!")
            }
            else
            {
                let annotations = self.mapViewOutlet.annotations
                self.mapViewOutlet.removeAnnotations(annotations)
                
                let latitude = responce?.boundingRegion.center.latitude
                let longitude = responce?.boundingRegion.center.longitude
                
                let annotation = MKPointAnnotation()
                annotation.title = searchBar.text
                annotation.coordinate = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
                self.mapViewOutlet.addAnnotation(annotation)
                
                let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude!, longitude!)
                let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                let region = MKCoordinateRegion(center: coordinate, span: span)
                self.mapViewOutlet.setRegion(region, animated: true)
                
            }
        }
        
        let urlString = "https://api.apixu.com/v1/current.json?key=bf21bf36247c4b3a878133001191107&q=\(searchBar.text!.replacingOccurrences(of: " ", with: "%20"))"
        let url = URL(string: urlString)
        var locationName: String?
        var temperature: Double?
         var feelsLike: Double?
        let task = URLSession.shared.dataTask(with: url!) {[weak self] (data, responce, error) in
            do {
                let json = try JSONSerialization.jsonObject(with: data! , options: .mutableContainers) as! [String : AnyObject]
                if let location = json["location"]  {
                    locationName = location["name"] as? String
                }
                if let current = json["current"] {
                    temperature = current["temp_c"] as? Double
                    feelsLike = current["feelslike_c"] as? Double
                }
                
                DispatchQueue.main.async {
                    
                    self?.cityLabel.text = locationName
                    if let temperature = temperature {
                        self?.temperatureLabel.text = "\(temperature)"
                    }
                    if let feelsLike = feelsLike{
                        self?.feelsLikeLabel.text = "\(feelsLike)"
                    }
                }
                
            }
            catch let jsonError {
                print(jsonError)
            }
            
        }
        task.resume()
    }
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
}
